La branch par défaut (master) et les branches protégées (protected) sont toujours déployés au global.

Les branches de développement sont déployés par Merge Request Pipeline, c'est à dire que les change qui sont présents dans les rules prendront en compte le dernier commit de la branche master, rebase inclus.

La branche par défaut qui garde un comportement classique, a un full_pipeline au lancement (pas de commit de référence vu qu'il n'y a pas de Merge Request d'associé).
A partir de ce moment là, les prochains triggers feront la diff avec le commit précédents.